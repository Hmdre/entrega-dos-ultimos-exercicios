/* 
1- Crie um programa em linguagem C que permita o usu�rio alocar dinamicamente um
vetor de inteiros com o tamanho que desejar, certifique-se de liberar a mem�ria
alocada no final do programa. 
*/
#include<stdio.h>
#include<stdlib.h>

int main()
{
    int n;
    int* vetor;
    printf("tamanho do vetor: ");
    scanf("d%", &n);
    vetor=(int*)malloc(sizeof(int)*n);
    free(vetor);
    return 0;
}
