/* 
2- Modifique o programa anterior para permitir que o usu�rio preencha o vetor com os
n�meros que desejar. 
*/
#include<stdio.h>
#include<stdlib.h>

int main()
{
    int n, a, aux;
    int* vetor;
    printf("tamanho do vetor: ");
    scanf("%d", &n);
    vetor=(int*)malloc(sizeof(int)*n);
    for(a=0;a<n;a++)
    {
        printf("De o valor para preencher a casa [%d] do vetor criado: ", a+1);
        scanf("%d", &vetor[a]);
    }
    free(vetor);
    return 0;
}
