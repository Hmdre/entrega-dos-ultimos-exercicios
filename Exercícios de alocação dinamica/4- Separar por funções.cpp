/* 
4-Modifique o programa anterior para que as funcionalidade de aloca��o,
preenchimento, exibi��o e libera��o, estejam isoladas em fun��es separadas.
*/
#include<stdio.h>
#include<stdlib.h>
void aloca_vetor(int* x, int** vet)
{
    printf("tamanho do vetor: ");
    scanf("%d", x);
    *vet=(int*) malloc(sizeof(int)*(*x));
}
void preenche_vetor(int x,int* vet)
{
    int z;
    for(z=0;z<x;z++)
    {
        printf("De o valor para preencher a casa [%d] do vetor criado: ", z+1);
        scanf("%d", &vet[z]);
    }
}
void printa_vetor(int x, int* vet)
{
    int z;
    printf("Vetor criado: ");
    for(z=0;z<x;z++)
    {
        printf("%d ", vet[z]);
    }
}
int main()
{
    int n, a;
    int* vetor;
    aloca_vetor(&n, &vetor);
    preenche_vetor(n,vetor);
    printa_vetor(n,vetor);
    free(vetor);
    return 0;
}
