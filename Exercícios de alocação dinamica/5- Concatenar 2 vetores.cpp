/*
5- Escreva um programa que crie dois vetores (com tamanhos e calores escolhidos pelo usu�rio, e alocados
dicamicamente) e um terceiro (tamb�m alocado dinamicamente) que seja a concatena��o dos dois primeiros.
Exiba os tres vetores para o usu�rio e n�o se esqe�a da liberar toda a mem�ria alocada ao final da
execu��o. A concatena��o deve acontecer em uma fun��o isolada. Sugest�o: Use as fun��es criadas no
exerc�cio anterior.
*/
#include<stdio.h>
#include<stdlib.h>

void concatenar_vetores(int* vetor1, int* vetor2, int* vetor3, int x)
{
	int z;
	for(z=0;z<=x;z++)
	{
		vetor3[z]=vetor1[z]+vetor2[z];
	}
}

void aloca_vetor(int x, int** vet)
{
    *vet=(int*) malloc(sizeof(int)*x);
}

void preenche_vetor(int x,int* vet)
{
    int z;
    for(z=0;z<x;z++)
    {
        printf("De o valor para preencher a casa [%d] do vetor: ", z+1);
        scanf("%d", &vet[z]);
    }
}

void printa_vetor(int x, int* vet)
{
    int z;
    for(z=0;z<x;z++)
    {
        printf("%d ", vet[z]);
    }
    printf("\n");
}

int main()
{
	int* vet1;
	int* vet2;
	int* vet3;
	int n;
	printf("Escolha um tamanho para criar 2 vetores: ");
	scanf("%d",&n);
	aloca_vetor(n, &vet1);
	aloca_vetor(n, &vet2);
	aloca_vetor(n, &vet3);
	printf("Preencha o primeiro vetor criado.\n");
	preenche_vetor(n,vet1);
	printf("Preencha o segundo vetor criado.\n");
	preenche_vetor(n,vet2);
	printf("Primeiro vetor:\n");
	printa_vetor(n,vet1);
	printf("Segundo vetor:\n");
	printa_vetor(n,vet2);
	printf("Concatenacao dos vetores:\n");
	concatenar_vetores(vet1,vet2,vet3,n);
	printa_vetor(n,vet3);
	free(vet1);
	free(vet2);
	free(vet3);
	return 0;
}
