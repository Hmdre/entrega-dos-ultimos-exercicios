/*
2-Preencha cada c�lula da matriz anterior com a soma de seus �ndices;
*/
#include<stdio.h>
#include<stdlib.h>

int main()
{
    int** matriz;
    int dx, dy, cont1, cont2;
    printf("Entre com a quantidade de linhas da matriz: ");
    scanf("%d",&dx);
    printf("Entre com a quantidade de colunas da matriz: ");
    scanf("%d",&dy);
    matriz=(int**)malloc(dx*sizeof(int*));
    for(cont1=0;cont1<dx;cont1++)
    {
        matriz[cont1]=(int*)malloc(dy*sizeof(int*));
    }
    for(cont1=0;dx>cont1;cont1++)
    {
        for(cont2=0;dy>cont2;cont2++)
        {
            matriz[cont1][cont2]=cont1+cont2;
        }
    }
    for(cont1=0;dx>cont1;cont1++)
    {
        printf("\n");
        for(cont2=0;dy>cont2;cont2++)
        {
            printf("%d  ",matriz[cont1][cont2]);
        }
    }
    
    return 0;
}
