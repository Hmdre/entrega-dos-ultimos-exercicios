/**************************

4-Modifique o programa anterior para que se tenha um menu onde o usu�rio pode
alterar valor das casas, realocar o tamanho da matriz, imprimir ou sair;

***************************/
#include<stdio.h>
#include<stdlib.h>

void Alterar_Conteudo(int dx, int dy,int** matriz)
{
    int cont1, cont2;
    for(cont1=0;dx>cont1;cont1++)
    {
        for(cont2=0;dy>cont2;cont2++)
        {
            printf("Conteudo da linha %d e coluna %d: ",cont1+1,cont2+1);
            scanf("%d",&matriz[cont1][cont2]);
        }
    }
    printf("\n");
}

void Realocar_Matriz(int* dx, int* dy, int*** matriz)
{
    int cont1;
    printf("Tamanho atual: %d linhas - %d colunas\n", *dx, *dy);
    *matriz=(int**)realloc(*matriz,*dx*sizeof(int*));
    for(cont1=0;cont1<*dx;cont1++)
    {
        (*matriz)[cont1]=(int*)realloc((*matriz)[cont1],*dy*sizeof(int));
    }
}

void Imprimir_Matriz(int dx, int dy, int** matriz)
{
    int cont1, cont2;
    for(cont1=0;dx>cont1;cont1++)
    {
        printf("\n");
        for(cont2=0;dy>cont2;cont2++)
        {
            printf("%d  ",matriz[cont1][cont2]);
        }
    }
    printf("\n");
}

int main()
{
    int b, x, y, z=-1;
    int** matriz;
    printf("Entre com a quantidade de linhas da matriz: ");
    scanf("%d",&x);
    printf("Entre com a quantidade de colunas da matriz: ");
    scanf("%d",&y);
    matriz=(int**)malloc(x*sizeof(int*));
    for(b=0;b<x;b++)
    {
        matriz[b]=(int*)malloc(y*sizeof(int));
    }
    
    while(z!=0)
    {
        printf("\n1-Alterar Conteudo da matriz.\n2-Aumentar as dimens0es da matriz em 1.\n3-Mostrar matriz.\n0-Encerrar Programa.\nDigite o numero da funcao desejada: ");
        scanf("%d",&z);
        if(z==1)
        {
            Alterar_Conteudo(x,y,matriz);
        }
        if(z==2)
        {
        	x = x + 1;
    		y = y + 1;
            Realocar_Matriz(&x,&y,&matriz);
        }
        if(z==3)
        {
            Imprimir_Matriz(x,y,matriz);
        }
        if(z!=1&&z!=2&&z!=3&&z!=0)
        {
            printf("Valor invalido");
        }
    }
    return 0;
}
