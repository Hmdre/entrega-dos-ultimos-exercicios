/*
6- Escreve um programa recursivo que utilize a fun��o: potencia(base, expoente) onde o programa
recebe a base e o expoente e retorne o resultado da pot�ncia. OBS: o expoente n�o pode ser menor que 1.
*/
#include<stdio.h>
int potencia(int base, int expoente)
{
	if(expoente==2)
	{
		return base*base;
	}
	else
	{
		return (base*potencia(base,expoente-1));	
	}
	
}
int main()
{
	int b, e;
	printf("Entre com o numero da base: ");
	scanf("%d",&b);
	printf("Entre com um expoente valido (maior que 1): ");
	scanf("%d",&e);
	if(e<1)
	{
		printf("Valor invalido!");
	}
	else
	{
		printf("O resultado da potencia e: %d",potencia(b,e));
	}
	return 0;
}

