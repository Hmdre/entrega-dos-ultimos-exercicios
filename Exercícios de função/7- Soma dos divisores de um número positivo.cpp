/*
7- Escreva um programa que leia um n�mero inteiro positivo (utilize uma fun��o que leia o n�mero e verifique se ele � positivo)
e mostre na tela a soma de seus divisores (exceto ele mesmo). Exemplo: para o valor 8: 1+2+4=7
*/
#include<stdio.h>
void verificar_positivo(int n)
{
	int x, soma=0;
	if(n<=0)
	{
		printf("O numero %d nao e positivo.",n);
	}
	else
	{
		for(x=n-1;x>=1;x--)
		{
			if(n%x==0)
			{
				soma+=x;
			}
		}
	printf("A soma dos divisores de %d e: %d",n ,soma);
	}
}
int main()
{
	int num;
	printf("Entre com um numero inteiro e positivo: ");
	scanf("%d",&num);
	verificar_positivo(num);
}

