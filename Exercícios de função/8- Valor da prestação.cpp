/*
8- Fa�a um programa em "C" que solicita o total gasto pelo cliente de uma loja, imprime as op��es de pagamento, solicita a op��o desejada e
imprime o valor total das presta��es (se houverem).
 1) Op��o: a vista com 10% de desconto
 2) Op��o: em duas vezes (pre�o da etiqueta)
 3) Op��o: em 10 vezes com 3% de juros ao m�s (somente para compras acima de R$ 150,00).
 */
#include<stdio.h>
void valor_prestacao(float n,int o)
{
	int a;
	switch (o)
	{
		case 1:
		printf("Total a pagar: %.2f",n-n*0.1);
		break;
		
		case 2:
		printf("Total a pagar: %.2f",n/2);
		break;
		
		case 3:
		printf("Em quantas vezes deseja parcelar?: ");
		scanf("%d",&a);
		if(a>2&&n>150)
		{
			printf("O valor da prestacao e de: %.2f",(n/a)+(n*0.03));
		}
		else
		{
			printf("O numero de parcelas e/ou o valor da compra nao correspondem com a opcao escolhida.");
		}
		break;
		
		default :
    	printf ("Valor para opcao invalido!\n");
	}
	
}
int main()
{
	int opcao;
	float valor;
	printf("Valor total da compra: ");
	scanf("%f",&valor);
	printf("Opcoes de pagamento:\n1) Opcao: a vista com 10 porcento de desconto\n2) Opcao: em duas vezes (pre�o da etiqueta)\n3) Opcao: em 10 vezes com 3 porcento de juros ao mes (somente para compras acima de R$ 150,00).\nDigite o numero da opcao desejada: ");
	scanf("%d",&opcao);
	valor_prestacao(valor,opcao);
	return 0;
}
