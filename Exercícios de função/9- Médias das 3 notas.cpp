/*
9- Um professor, muito legal, fez 3 provas durante um semestre mas s� vai levar em conta as duas notas mais altas para calcular a m�dia.
Fa�a uma aplica��o em C que pe�a o valor das 3 notas, mostre como seria a m�dia com essas 3 provas, a m�dia com as 2 notas mais altas,
bem como sua nota mais alta e sua nota mais baixa.
*/
#include<stdio.h>
void media(float maior, float medio, float menor)
{
	printf("A nota mais alta foi: %.2f\n", maior);
	printf("A nota media foi: %.2f\n", medio);
	printf("A media com as tres provas fica em: %.2f\n", (maior+medio+menor)/3);
	printf("A media com as duas provas de maior nota fica em: %.2f\n", (maior+medio)/2);
}
int main()
{
	float a, b, c, aux;
	printf("Entre com a nota da primeira prova: ");
	scanf("%f",&a);
	printf("Entre com a nota da segunda prova: ");
	scanf("%f",&b);
	printf("Entre com a nota da terceira prova: ");
	scanf("%f",&c);
	if(c>b)
	{
		aux=b;
		b=c;
		c=aux;
	}
	if(b>a)
	{
		aux=a;
		a=b;
		b=aux;
	}
	if(c>b)
	{
		aux=b;
		b=c;
		c=aux;
	}
	media(a,b,c);
	return 0;
}
