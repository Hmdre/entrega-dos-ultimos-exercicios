/*
2- Diga qual o maior valor entre dois n�meros recebidos pelo usu�rio, usando ponteiros
*/
#include<stdio.h>

int maior_valor(int *x, int *y)
{
	if(*x>*y)
	{
		return (*x);
	}
	else
	{
		return (*y);
	}
}

int main()
{
    int a,b,soma;
    printf("Entre com dois valores: ");
    scanf("%d %d",&a,&b);
	printf("O maior valor e: %d",maior_valor(&a,&b));
    return 0;
}
