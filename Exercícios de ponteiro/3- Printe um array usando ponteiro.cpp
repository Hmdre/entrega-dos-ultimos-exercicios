/*
3- Receba valores de um array tamanho 5, e depois os printe, usando ponteiros.
*/
#include<stdio.h>
 
int main()
{
	int n, vet[5];
	int* aponta_vet;
	aponta_vet=vet;
	for(n=0;n<5;n++)
	{
		printf("Entre com o valor da casa [%d] do vetor: ",n+1);
		scanf("%d",&vet[n]);
	}
	printf("Vetor criado: ");
    for(n=0;n<5;n++)
    {
        printf("%d ", *(aponta_vet+n));
    }
    return 0;
}
