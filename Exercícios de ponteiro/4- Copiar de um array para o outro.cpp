/*
4- Copie os valores de um array, tamanho 5, para outro usando ponteiros.
*/
#include<stdio.h>
int main()
{
	int n, vet1[]={3,6,7,2,1}, vet2[5];
	int* aponta_vet;
	aponta_vet=vet1;
    for(n=0;n<5;n++)
    {
        vet2[n]=*(aponta_vet+n);
    }
    for(n=0;n<5;n++)
    {
        printf("%d ", vet2[n]);
    }
    return 0;
}
