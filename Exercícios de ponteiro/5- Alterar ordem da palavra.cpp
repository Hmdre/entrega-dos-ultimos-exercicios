/*
5- Mostre o inverso de uma palavra inserida pelo usu�rio, usando ponteiros.
*/
#include <stdio.h>
void alterar_ordem(char* word,int x)
{
	int a, aux;
	for(a=0;a<x/2;a++)
	{
		aux=word[a];
		word[a]=word[(x-1)-a];
		word[(x-1)-a]=aux; 	
	}
	for(a=0;a<x;a++)
	{
		printf("%c",word[a]);
	}
}

int main()
{
	int n=7;
	char word[n]={"palavra"};
	alterar_ordem(word,n);
	return 0;
}
