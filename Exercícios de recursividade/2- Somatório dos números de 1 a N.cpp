/*
Crie uma fun��o recursiva que receba um n�mero inteiro positivo N e calcule o somat�rio
dos n�meros de 1 a N.
*/
#include<stdio.h>

int somaN(int soma)
{
    if(soma==1)
    {
        return(soma);
    }
    else
    {
        return(soma+somaN(soma-1));
    }
}
int main()
{
    int n;
    printf("Entre com um numero natural positivo: ");
    scanf("%d",&n);
    printf("O somatorio dos numeros naturais ate %d e : %d",n,somaN(n));
    return 0;
}
