/*
3- Calcule a soma de todos os valores de um array.
*/
#include<stdio.h>

int somavet(int *vet, int n)
{
    if(n==0)
    {
        return(vet[n]);
    }
    else
    {
        return(vet[n]+somavet(vet,n-1));
    }
}
int main()
{
    int vet[4], i;
    for(i=0;i<4;i++)
    {
    	printf("Entre com o valor da casa %d do vetor: ",i+1);
    	scanf("%d",&vet[i]);
	}
    printf("A soma dos valores do vetor e: %d",somavet(vet,3));
    return 0;
}
