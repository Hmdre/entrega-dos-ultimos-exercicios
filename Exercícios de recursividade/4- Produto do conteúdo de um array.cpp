/*
4- Calcule o produto de todos os valores de um array.
*/
#include<stdio.h>

int produto_vet(int *vet, int n)
{
    if(n==0)
    {
    	
        return(vet[n]);
    }
    else
    {
        return(vet[n]*produto_vet(vet,n-1));
    }
}
int main()
{
    int vet[4], i;
    for(i=0;i<4;i++)
    {
    	printf("Entre com o valor da casa %d do vetor: ",i+1);
    	scanf("%d",&vet[i]);
	}
    printf("O produto dos valores do vetor e: %d",produto_vet(vet,3));
    return 0;
}
