/*
5- Fa�a uma fun��o recursiva que receba um n�mero inteiro positivo par N e imprima todos os n�meros
pares de 0 at� N.
*/
#include<stdio.h>

void imprimir_pares(int n, int x)
{
    if(n==x)
    {
        printf("%d ",x);
    }
    else
    {
    	printf("%d ",x);
    	imprimir_pares(n,x+2);
    }
}
int main()
{
    int num;
    printf("Entre com um numero inteiro positivo: ");
    scanf("%d",&num);
    if(num%2==0)
    {
    	imprimir_pares(num,0);
	}
	else
	{
		num-=1;
		imprimir_pares(num,0);
	}
    return 0;
}
