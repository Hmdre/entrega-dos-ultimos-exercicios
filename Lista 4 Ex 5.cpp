/* Fa�a uma fun��o que recebe, por par�metro, a altura (alt) e o sexo de uma pessoa e retorna o seu peso ideal. 
Para homens, calcular o peso ideal usando a f�rmula peso ideal = 72.7 x alt - 58 e, para mulheres,
peso ideal = 62.1 x alt - 44.7.*/
#include<stdio.h>
float Peso_Ideal(float h,int g)
{
	switch (g)
	{
   		case 1:
    		h = 72.7*h-58;
   		break;

   		case 2:
     		h = 62.1*h-44.7;
   		break;

   		default:
   			h=0;
     		printf ("Valor para genero invalido!\n");
   	}
	return 	(h);
}
int main()
{
	int genero=0;
	float altura=0;
	printf("De sua altura em metros: ");
	scanf("%f",&altura);
	printf("Selecione seu sexo:\nDigite 1 para masculino ou 2 para feminino: ");
	scanf("%d",&genero);
	altura=Peso_Ideal(altura,genero);
	if(altura!=0){
		printf("Seu Peso Ideal: %.2f",altura);
	}
	return 0;
}
