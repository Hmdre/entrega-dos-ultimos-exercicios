/*
1- ) Fa�a um programa que leia o tamanho de um vetor de inteiros e reserve dinamicamente memoria para esse vetor. 
Em seguida, leia os elementos desse vetor, imprima o vetor lido e mostre o resultado da soma dos numeros �mpares
presentes no vetor.
*/
#include<stdio.h>
#include <stdlib.h>
int main()
{
	int *vet, c, n, soma_impares=0, a;
	printf("De o numero de espa�os que deseja no vetor: ");
	scanf("%d",&n);
	vet=(int*)malloc(n*sizeof(int));
	for (c=0;c<n;c++)
	{
    	printf("De o valor para a posicao %d do vetor: ", c+1);
    	scanf("%d",&vet[c]);
	}
	for (c=0;c<n;c++)
	{
    	printf("%d ",vet[c]);
    	a=vet[c]%2;
    	if(a==1)
    		{
    			soma_impares+=vet[c];
			}
	}
	printf("\nA soma dos valores impares do vetor e igual a: %d",soma_impares);
	free(vet);
	return 0;
}
